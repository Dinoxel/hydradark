<?php
/**
 * SkinTemplate class for HydraDark skin
 * @ingroup Skins
 */
class SkinHydraDark extends SkinHydra {
	public $skinname = 'hydradark';
	public $stylename = 'HydraDark';
	public $template = 'HydraDarkTemplate';

	/**
	 * Loads skin and user CSS files.
	 *
	 * @param OutputPage $out
	 */
	public function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		$out->addModuleStyles(
			[
				'skins.z.hydra.dark.styles'
			]
		);
		if (ExtensionRegistry::getInstance()->isLoaded('SyntaxHighlight')) {
			$out->addModuleStyles(
				[
					'skins.z.hydra.dark.syntaxhighlight.monokai.styles'
				]
			);
		}
	}
}
